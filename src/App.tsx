import React from 'react';
import logo from './logo.svg';
import './App.css';
import DragDrop from './Component/DragAndDrop'

function App() {
  return (
    <div>
      <DragDrop></DragDrop>
    </div>
  );
}

export default App;
