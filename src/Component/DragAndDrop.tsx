
import React from 'react';
import {useDropzone} from 'react-dropzone';
import '../CSS/DragDrop.css'

export default function DragAndDrop(props:any) {
  const {getRootProps, getInputProps, open, acceptedFiles} = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
 
  });
  

  const files = acceptedFiles.map(file => (
    <li key={file.name}>
      {file.name} - {file.type} bytes
    </li>
  ));

  

  return (
    <div className='wholePage'>
      <div className='root'>
      <div {...getRootProps({className: 'dropzone'})}>
        <input {...getInputProps()} />
        <div className='dragDrop'>
        <p >Drag and drop your files here</p>
        <button className='dragButton' type="button" onClick={open}>
          File From Your Device
        </button>
        </div>
      </div>
      </div>
      <div className='files'>
        <h4>Files appear here</h4>
        <ul>{files}</ul>
      </div>
    </div>
  );
}

